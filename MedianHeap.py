class MedianHeap:
    heapFirst = []
    heapLast = []
    median = 0.0
    def __init__(self):
        self.median = 0.0
        self.heapFirst = []
        self.heapLast = []
    def Push(self,num):
        if num >= self.median:
            self.HeapInsert(self.heapLast,num,-1)
            #print 'insert done',num,self.heapLast
        else:
            self.HeapInsert(self.heapFirst,num,1)
            #print 'insert done',num,self.heapFirst
        #see if need relocate
        posInd = len(self.heapLast) - len(self.heapFirst)
        if posInd == 2:
            temp = self.HeapExtract(self.heapLast,-1)
            self.HeapInsert(self.heapFirst,temp,1)
        elif posInd == -2:
            temp = self.HeapExtract(self.heapFirst,1)
            self.HeapInsert(self.heapLast,temp,-1)
        else:
            pass
        posInd = len(self.heapLast) - len(self.heapFirst)
        #compute median
        if posInd == 1:
            self.median = self.heapLast[0]
        elif posInd == -1:
            self.median = self.heapFirst[0]
        elif posInd == 0:
            #self.median = (self.heapFirst[0] + self.heapLast[0])/2.0
            
            self.median = self.heapFirst[0] #due to the requirement
        else:
            print 'something is wrong'
    def Swap(self,heap,i,j):
        temp = heap[i]
        heap[i] = heap[j]
        heap[j] = temp
    def HeapInsert(self,heap,num,id):
        heap.append(num)
        i = len(heap)-1
        while (i > 0 and (heap[i] - heap[(i+1)/2-1])*id > 0):
            self.Swap(heap,i,(i+1)/2-1)
            i = (i+1)/2-1
    def HeapExtract(self,heap,id):
        mini = heap[0]
        try: heap[0] = heap.pop()
        except: 
            heap.pop()
            return mini
        i = 0
        while True:
            try:
                if ((heap[2*i+1] - heap[i])*id > 0 and \
                        (heap[2*i+1] - heap[2*i+2])*id >= 0):
                    self.Swap(heap,i, 2*i+1)
                    i = 2*i+1
                elif ((heap[2*i+2] - heap[i])*id > 0 and \
                        (heap[2*i+2] - heap[2*i+1])*id >= 0):
                    self.Swap(heap,i, 2*i+2)
                    i = 2*i+2
                else: break
            except:
                try:
                    if ((heap[2*i+1] - heap[i])*id > 0):
                        self.Swap(heap,i, 2*i+1)
                        i = 2*i+1
                    else: break
                except: break
        return mini